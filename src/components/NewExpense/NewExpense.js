import React, { useState } from 'react';
import ExpenseForm from './ExpenseForm';
import './NewExpense.css';

export default function NewExpense(props) {

    const [isEditting, setisEditting] = useState(false);

    const saveExpenseDataHandler = (enteredExpenseData) => {
        const expenseData = {
            ...enteredExpenseData,
            id: Math.random().toString()
        };
        props.onAddExpense(expenseData);
        setisEditting(false);
    };

    const startEdittingHandler = () => {
        setisEditting(true);
    };

    const stopEdittingHandler = () => {
        setisEditting(false);
    };


    return (
        <div className="new-expense">
            {!isEditting && (<button onClick={startEdittingHandler}>Add new expense</button>)}
            {isEditting && (<ExpenseForm onCancel={stopEdittingHandler} onSaveExpenseData={saveExpenseDataHandler} />)}

        </div>
    )
}
