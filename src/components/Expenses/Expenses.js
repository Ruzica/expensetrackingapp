import React, { useState } from 'react';
import ExpensesFilter from './ExpensesFilter';
import Card from '../UI/Card';
import './Expenses.css';
import ExpensesList from './ExpensesList';
import ExpensesChart from './ExpensesChart';

export default function Expenses(props) {
    const [selectedYear, setSelectedYear] = useState('2020');

    const selectedYearHandler = (selectedYear) => {
        setSelectedYear(selectedYear);
    }

    const filteredByYearExpenses = props.expenses.filter((expense) => {
        return expense.date.getFullYear().toString() === selectedYear;
    });

    return (
        <Card className="expenses">
            <ExpensesFilter selected={selectedYear} onSelectedYear={selectedYearHandler} />
            <ExpensesChart expenses={filteredByYearExpenses} />
            <ExpensesList expenses={filteredByYearExpenses} />
        </Card>
    )
}

