import { useState } from 'react';
import './App.css';
import Expenses from './components/Expenses/Expenses';
import NewExpense from './components/NewExpense/NewExpense';

const expensesData = [
  {
    title: 'Expense 1',
    amount: '120.56',
    date: new Date(2022, 5, 12),
    id: 0
  },

  {
    title: 'Expense 2',
    amount: '150.88',
    date: new Date(2022, 2, 10),
    id: 1
  }
];

function App() {

  const [expenses, setExpenses] = useState(expensesData);


  const addExpenseHandler = (expense) => {
    console.log('In App.js');
    console.log(expense);

    setExpenses((prevExpenses) => {
      return [expense, ...prevExpenses];
    });
  };

  return (
    <div>
      <NewExpense onAddExpense={addExpenseHandler} />
      <Expenses expenses={expenses} />
    </div>
  );
}

export default App;
